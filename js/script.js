// * Activate Menu Links
const links = document.querySelectorAll(".header-menu a");

links.forEach((link) => {
  const url = location.href;
  const href = link.href;
  if (url.includes(href)) {
    link.classList.add("ativo");
  }
});

// * Activate budget items

const parametros = new URLSearchParams(location.search);

parametros.forEach((itens) => {
  const element = document.getElementById(itens);
  if (element) element.checked = true;
});

// * Bike Gallerys

const gallery = document.querySelectorAll(".colors-bike img");
const galleryContainer = document.querySelector(".colors-bike");

const switchImg = (e) => {
  const img = e.currentTarget;
  const media = matchMedia("(min-width:470px)").matches;
  if (media) galleryContainer.prepend(img);
};

gallery.forEach((img) => {
  img.addEventListener("click", switchImg);
});

// * Frequents Questions

const questions = document.querySelectorAll(".questions button");

const activedQuestions = (event) => {
  const buttonQuestions = event.currentTarget;
  const controls = buttonQuestions.getAttribute("aria-controls");
  const answer = document.getElementById(controls);
  answer.classList.toggle("actived");
  const toggleExpanded = answer.classList.contains("actived");
  buttonQuestions.setAttribute("aria-expanded", toggleExpanded);
};

questions.forEach((questions) => {
  questions.addEventListener("click", activedQuestions);
});

// * Animation
if (window.SimpleAnime) new SimpleAnime();
